class MyCache
  cattr_accessor :users, :wall_items, :messages
  @@users = []
  @@wall_items = []
  @@messages = []

  def self.find_user(login)
    self.users.detect {|user| user.login == login }
  end

  def self.find_wall_item(id)
    self.wall_items.detect {|user| user.id == id }
  end

  @@user_id = 0
  @@wall_item_id = 0
  @@message_id = 0

  def self.new_user_id
    @@user_id += 1
  end

  def self.new_wall_item_id
    @@wall_item_id += 1
  end

  def self.new_message_id
    @@message_id += 1
  end
end