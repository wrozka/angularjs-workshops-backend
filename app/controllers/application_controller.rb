class ApplicationController < ActionController::Base
  @@users = []

  protect_from_forgery

  def validation_failed(message)
    render :json => { :invalid => message }, :status => 400
  end
end
