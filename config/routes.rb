Workshop::Application.routes.draw do
  namespace :api do
    resources :users, only: [:index, :create, :destroy] do
      member do
        post :like
        post :invite
      end
    end
    resources :wall_items, only: [:create, :index, :show, :destroy]
  end
end
